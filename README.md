Enemic: Crear un prefab amb un element visual dels que es proporcionen. Aquest objecte enemic quan sigui a l'escena caminarà en direcció al Player.
(Bonus) Prefab amb animacions

Mort: si el enemic toca al jugador aquest  l'enemic s'elimina i el player perd una vida. Si player perd dos vides s'acaba el joc. Si el joc s'acaba torna a iniciar-se la partida.
(Bonus) Si l'enemic toca a un altre enemic els dos s'eliminen.

Spawner: objecte en escena que genera enemics.  
(Bonus) Spawner: Aquest objecte invoca enemics als marges de la pantalla.

UI: comptador de vides. Comptador d'enemics a l'escena. Comptador de punts per enemic 5.

GameManager: que gestiona l'estat del joc, el reinici de partida i serveix per comunicar els elements de partida.

Matar l'enemic: si el jugador fa click amb el ratolí en un enemic aquest s'elimina.

